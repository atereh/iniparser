package com.company;

//import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.AbstractMap;

public class Main {

    public static void main(String[] args) {
        File file = new File("example.ini");
        IniParser parser = IniParser.getInstance();
        //IniParser parser2 = new IniParser();
        parser.parseIniFile(file);

        System.out.println(parser.<Double>getParameterFromSection("SampleRate", "ADC_DEV", Double.class));
        System.out.println(parser.<Integer>getParameterFromSection("SampleRate", "ADC_DEV", Integer.class));
        System.out.println(parser.<String>getParameterFromSection("SampleRate", "ADC_DEV", String.class));
        System.out.println();

        System.out.println(parser.<Double>getParameterFromSection("SampleRate", "NCMD", Double.class));
        System.out.println(parser.<Integer>getParameterFromSection("SampleRate", "NCMD", Integer.class));
        System.out.println(parser.<String>getParameterFromSection("SampleRate", "NCMD", String.class));
        System.out.println();

        System.out.println(parser.<Double>getParameterFromSection("ListenTcpPort", "LEGACY_XML", Double.class));
        System.out.println(parser.<Integer>getParameterFromSection("ListenTcpPort", "LEGACY_XML", Integer.class));
        System.out.println(parser.<String>getParameterFromSection("ListenTcpPort", "LEGACY_XML", String.class));
        System.out.println();

        System.out.println(parser.<Double>getParameterFromSection("DiskCachePath", "COMMON", Double.class));
        System.out.println(parser.<Integer>getParameterFromSection("DiskCachePath", "COMMON", Integer.class));
        System.out.println(parser.<String>getParameterFromSection("DiskCachePath", "COMMON", String.class));
        System.out.println();

        System.out.println(parser.<String>getParameterFromSection("Parameter", "Section", String.class));
        System.out.println(parser.<String>getParameterFromSection("Parameter", "NCMD", String.class));

        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        try {
            String section, param;
            do {
                section = buffer.readLine();
                if(section.equals("exit")) break;
                param = buffer.readLine();
                AbstractMap.SimpleEntry<String, String> result = parser.getParameterFromSection(param, section);
                System.out.println("Value: "+result.getKey()+"\n"+"Comment: "+result.getValue());
            } while (!section.equals("exit"));
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
