package com.company;

public class FileFormatException extends Exception{
    FileFormatException(String message) {
        super(message);
    }
}
