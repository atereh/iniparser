package com.company;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class IniPreference {
    private String name;
    private Map<String, AbstractMap.SimpleEntry<String, String>> mapLine = new HashMap<>();

    IniPreference() { }

    IniPreference(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    Map<String, AbstractMap.SimpleEntry<String, String>> getMapLine() {
        return mapLine;
    }

    void setName(String name) {
        this.name = name;
    }

    void addLine(String key, String value, String comment) {
        mapLine.put(key, new AbstractMap.SimpleEntry<String, String>(value, comment));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IniPreference)) return false;
        IniPreference iniPreference = (IniPreference) o;
        return Objects.equals(getName(), iniPreference.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
