package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class IniParser {
    private static IniParser ourInstance = new IniParser();

    private List<IniPreference> sectionList = new ArrayList<>();

    public static IniParser getInstance() {
        return ourInstance;
    }

    public IniParser() {
    }

    void parseIniFile(File file) {
        try (Scanner scanner = new Scanner(file)) {
            if (!file.getName().matches("\\w+\\.ini")) {
                throw new FileFormatException("Incorrect file format");
            }
            while (scanner.hasNextLine()) {
                String preferenceName = scanner.nextLine();
                while (scanner.hasNextLine() && !preferenceName.matches("\\[\\w+\\]")) {
                    preferenceName = scanner.nextLine();
                }
                IniPreference iniPreference = new IniPreference();
                if (preferenceName.matches("\\[\\w+\\]")) {
                    preferenceName = preferenceName.substring(1, preferenceName.length() - 1);
                    iniPreference.setName(preferenceName);
                }
                String line;
                while (scanner.hasNextLine() && !(line = scanner.nextLine()).trim().isEmpty() &&
                        !line.substring(0, 1).equals(";")) {
                    //String[] words = line.split(" ");
                    String[] words = line.split("=");
                    String[] value = words[1].split(";");
                    String comment = "";
                    if(value.length > 1) comment = value[1].trim();
                    iniPreference.addLine(words[0].trim(), value[0].trim(), comment);
                }
                sectionList.add(iniPreference);
            }
        } catch (FileNotFoundException | FileFormatException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    <T> T getParameterFromSection(String parameter, String iniPreference, Class<T> type) {
        int index = sectionList.indexOf(new IniPreference(iniPreference));
        try {
            if (index != -1) {
                Map<String, AbstractMap.SimpleEntry<String, String>> map = sectionList.get(index).getMapLine();
                if (map.containsKey(parameter)) {
                    AbstractMap.SimpleEntry<String,String> pair = map.get(parameter);
                    String value = pair.getKey();
                    if (type == Integer.class) {
                        if (value.matches("\\d+")) {
                            return (T) Integer.valueOf(value);
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else if (type == Double.class) {
                        if (value.matches("\\d+\\.\\d+")) {
                            return (T) Double.valueOf(value);
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else if (type == String.class) {
                        if (!value.matches("\\d+") && !value.matches("\\d+\\.\\d+")) {
                            return (T) value;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    }
                } else {
                    throw new ElementNotFoundException("Parameter doesn't exist");
                }
            } else {
                throw new ElementNotFoundException("Section doesn't exist");
            }
        } catch (IllegalArgumentException | ElementNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    AbstractMap.SimpleEntry<String, String> getParameterFromSection(String parameter, String iniPreference) {
        int index = sectionList.indexOf(new IniPreference(iniPreference));
        try {
            if (index != -1) {
                Map<String, AbstractMap.SimpleEntry<String, String>> map = sectionList.get(index).getMapLine();
                if (map.containsKey(parameter)) {
                    AbstractMap.SimpleEntry<String,String> pair = map.get(parameter);
                    return pair;
                    } else {
                    throw new ElementNotFoundException("Parameter doesn't exist");
                }
            } else {
                throw new ElementNotFoundException("Section doesn't exist");
            }
        } catch (IllegalArgumentException | ElementNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
