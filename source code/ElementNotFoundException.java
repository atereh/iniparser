package com.company;

public class ElementNotFoundException extends Exception {
    ElementNotFoundException(String message) {
        super(message);
    }
}
