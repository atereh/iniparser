# IniParser

This project provides solving an academic task: create a tool for processing the configuration of INI file.

### Details: 
Describe and implement classes that will allow processing of the configuration file, which is a text file divided into SECTIONS that contain NAME and VALUE pairs. 
All parameters and section's names are strings without spaces, consisting of Latin characters, numbers, and underscores.
Section names are enclosed in square brackets, without spaces.
Parameter values are separated from parameter names by = (equality)

Parameter values can be one of the following types: 

- integer
- real
- string: without spaces, but unlike the parameter's name, it can contain also the "dot" symbol.


The file may contain comments. Anything after the semicolon is considered as a comment. Comments, like the semicolon itself, should be ignored.
The method **"get a value of a certain type with such a name from such a section"** must be implemented (for example, get the listentcpport integer from the LEGACY_XML section)

Errors must be handled:

- File subsystem error (for example, if the file is not found)
- File format error (if the file has the wrong format)
- Invalid parameter type (error when casting the type)
- The unspecified parameter SECTION, or pair is not in the configuration file and others, if necessary.

For implementation use Generics, collections, and exceptions.

### Contributors:
Alina Terekhova
